﻿Function Out-Log
{
    Param
    (
        [Parameter(Mandatory=$false)][string]$InfoMsg,
        [Parameter(Mandatory=$false)][string]$ErrMsg,
        [Parameter(Mandatory=$false)][string]$WarnMsg,
        [Parameter(Mandatory=$false)][switch]$NoFile
    )

    If ($InfoMsg) 
    {
        $Msg = $(Get-Date -Format HH-mm-dd-MM-yyyy) + " Info: " + $InfoMsg
        Write-Output $Msg
    }
    
    If ($WarnMsg) 
    {
        Write-Warning $Msg
        $Msg = $(Get-Date -Format HH-mm-dd-MM-yyyy) + " Warn: " + $InfoMsg

    }

    If ($ErrMsg) 
    {
        Write-Error $Msg
        $Msg = $(Get-Date -Format HH-mm-dd-MM-yyyy) + " Error: " + $InfoMsg

    }
    
    If (!$NoFile) 
    {
        If (!(Test-Path variable:global:LoggingPath)) 
        {
            Write-Error 'No Log file Set $LoggingPath'
        }
        Else
        {     
            $LogFileName = $(Get-Date -Format dd-MM-yyyy) + ".log"
            $LogFilePath = join-path $LoggingPath $LogFileName
            $Msg |Out-File $LogFilePath -Append ascii
        }
    }
}

Function Remove-oldLogs
{
    Param
    (
        [Parameter(Mandatory=$false)][string]$AgedDays
    )
    $OldFolder = Get-ChildItem -Path $LogRoot\*.log  |where {$_.CreationTime -lt (get-date).AddDays(-1)}

    ForEach ($Folder in $OldFolder)
    {
        Remove-Item $Folder.FullName
    }
}
